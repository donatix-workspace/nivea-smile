/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gms.samples.vision.face.facetracker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.samples.vision.face.facetracker.ui.camera.GraphicOverlay;
import com.google.android.gms.vision.face.Face;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final int ID_TEXT_SIZE = 40;
    private static final float BOX_STROKE_WIDTH = 10.0f;

    private static final int COLOR_CHOICES[] = {
            Color.WHITE
    };

    private static int mCurrentColorIndex = 0;
    private static Bitmap labelBeBetter;
    private static Bitmap labelHaveBeenBetter;
    private static Bitmap labelIAmHappy;
    private static Bitmap labelYourBestDay;
    private static Bitmap progress;
    private static Bitmap progressEmpty;

    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private Context context;

    FaceGraphic(Context context, GraphicOverlay overlay) {
        super(overlay);
        this.context = context;

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        initPaints();
        initBitmaps();

    }

    private void initBitmaps() {
//        labelHaveBeenBetter = BitmapFactory.decodeResource(context.getResources(),
//                R.drawable.have_been_better);
//        labelBeBetter = BitmapFactory.decodeResource(context.getResources(),
//                R.drawable.be_better);
//        labelIAmHappy = BitmapFactory.decodeResource(context.getResources(),
//                R.drawable.i_am_happy);
//        labelYourBestDay = BitmapFactory.decodeResource(context.getResources(),
//                R.drawable.your_best_day);
        progress = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.progress_bar);
        progressEmpty = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.progress_bar_empty);
    }

    private void initPaints() {
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mIdPaint = new Paint();
        mIdPaint.setColor(Color.WHITE);
        mIdPaint.setAntiAlias(true);
        mIdPaint.setStrokeWidth(2.0f);
        mIdPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mIdPaint.setShadowLayer(5.0f, 5.0f, 5.0f,
                ContextCompat.getColor(this.context, R.color.black_transparent));

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStrokeJoin(Paint.Join.ROUND);
        mBoxPaint.setStrokeCap(Paint.Cap.ROUND);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
        mBoxPaint.setAntiAlias(true);
    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if (face == null) {
            return;
        }

        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);

        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getWidth() / 2.5f);
        float yOffset = scaleY(face.getHeight() / 2.5f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        canvas.drawPath(createCornersPath(left, top, right, bottom, 50), mBoxPaint);
        drawProgress(face.getIsSmilingProbability(), canvas,left,top - 60, right-left, bottom-top);
    }

    private Path createCornersPath(float left, float top, float right, float bottom, float cornerWidth){
        Path path = new Path();

        path.moveTo(left, top + cornerWidth);
        path.lineTo(left, top);
        path.lineTo(left + cornerWidth, top);

        path.moveTo(right - cornerWidth, top);
        path.lineTo(right, top);
        path.lineTo(right , top + cornerWidth);

        path.moveTo(left, bottom - cornerWidth);
        path.lineTo(left, bottom);
        path.lineTo(left + cornerWidth, bottom);

        path.moveTo(right - cornerWidth, bottom);
        path.lineTo(right, bottom);
        path.lineTo(right, bottom - cornerWidth);


        return path;
    }

    private void drawProgress(float happiness, Canvas canvas, float left, float top, float right, float bottom) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setStrokeJoin(Paint.Join.ROUND);
        p.setStrokeCap(Paint.Cap.ROUND);

        float happinessRatio = Math.max(0, happiness);
        float progressCenter = left + right / 2;
        float halfProgressSize = 135;
        float progressLeft = progressCenter - halfProgressSize;
        float progressRight = progressLeft + (halfProgressSize * 2) * happiness;
        float redProgressStartSize = 25;
        float marginFromTop = 40;

        canvas.drawBitmap(progressEmpty, null, new RectF(progressLeft, top,
                progressCenter + halfProgressSize, top + marginFromTop), p);
        canvas.drawBitmap(progress, null, new RectF(progressLeft + redProgressStartSize, top,
                progressRight > progressLeft + redProgressStartSize ?
                        progressRight : progressLeft + redProgressStartSize, top + marginFromTop), p);

        float textY = top + 150 + bottom;
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/nivme.ttf");
        String[] label = getLabel(happinessRatio).split("\n");
        for (String line : label) {
            float xOffset = getApproxXToCenterText(line, typeface, ID_TEXT_SIZE, progressCenter);
            canvas.drawText(line, xOffset, textY, mIdPaint);
            textY += mIdPaint.descent() - mIdPaint.ascent();
        }
//        canvas.drawBitmap(getLabelBitmap(happinessRatio), null, new RectF(left + 20,
//          top + 100 + bottom, left + right - 20, bottom + 200 + top), mIdPaint);
    }

    private float getApproxXToCenterText(String text, Typeface typeface, int fontSize, float widthToFitStringInto) {

        mIdPaint.setTypeface(typeface);
        mIdPaint.setTextSize(fontSize);
        float textWidth = mIdPaint.measureText(text);
        return widthToFitStringInto - (textWidth / 2);
    }

    private String getLabel(float happinessRatio) {
        if(happinessRatio >= 0.0 && happinessRatio < 0.25) {
            return "Била съм и по-добре";
        }
        else if (happinessRatio > 0.25 && happinessRatio < 0.5) {
            return "Бъди по-ведра";
        }
        else if (happinessRatio > 0.5 && happinessRatio < 0.75) {
            return "Щастлива съм";
        }
        else if (happinessRatio > 0.75 && happinessRatio <= 1) {
            return "Днес е най-щастливият \nми ден";
        }
        return "Била съм и по-добре";
    }

    private Bitmap getLabelBitmap(float happinessRatio) {
        if(happinessRatio >= 0.0 && happinessRatio < 0.25) {
            return labelHaveBeenBetter;
        }
        else if (happinessRatio > 0.25 && happinessRatio < 0.5) {
            return labelBeBetter;
        }
        else if (happinessRatio > 0.5 && happinessRatio < 0.75) {
            return labelIAmHappy;
        }
        else if (happinessRatio > 0.75 && happinessRatio <= 1) {
            return labelYourBestDay;
        }
        return labelHaveBeenBetter;
    }
}
